# Central do Assinante O Dia / MH

Tecnologias usadas:

* PHP
* Composer

O projeto possui as seguintes dependências:

* PHPMailer
* DotEnv

O arquivo composer.lock não está sendo versionado. Assim sendo, o comando ```composer install``` deve ser executado toda vez que o arquivo *composer.json* for atualizado e/ou o sistema for instalado num novo servidor.

Existe um arquivo *.env* na raiz do projeto que guarda algumas configurações gerais. Dentre elas, existe a variável ***APP_URL***, que armazena o domínio do site.