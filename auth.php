<?php
require 'vendor/autoload.php';

use App\Controllers\Dissim;

$dissim = new Dissim;

if (!isset($_SESSION)) { session_start(); }

$result = $dissim->getContrato($_POST['email']);
$contrato = json_decode($result);

if (isset($_POST['btn']))  {

	if (empty($_POST['email'] && $_POST['senha'])) {
		echo "Preencha todos os campos!";
		exit();
	} else {
		if (isset($_POST['email']) && !empty($_POST['email'])) {
			$email = $_POST['email'];
		}

		if (isset($_POST['senha']) && !empty($_POST['senha'])) {
			$senha = $_POST['senha'];
		}

		$productId = $contrato[0]->CodProduto;

		$result = $dissim->WSAcessoAssinante($email, $senha, $productId);

		switch ($result) {
			case 'ATIVO':
				$_SESSION['email'] = $email;
				$_SESSION['senha'] = $senha;
				$_SESSION['productId'] = $productId;
				echo "Autenticado";
			break;

			case 'VENCIDO':
				echo 'Seu contrato está VENCIDO.';
			break;

			case 'INATIVO':
				echo 'Seu contrato está INATIVO.';
			break;

			default: 
				echo 'Sua credenciais estão incorretas.';
			break;
		}
	}
} else {
	echo 'Não houve requisição';
}