<?php
require 'vendor/autoload.php';
use App\Controllers\Dissim;
use PHPMailer\PHPMailer\PHPMailer;

if (!isset($_SESSION)) {
  session_start();
}

function enviaEmail($productId, $nome, $feedback, $msg, $telefone, $assinatura) 
{
  $mail = new PHPMailer;
  $mail->IsSMTP();
  $mail->CharSet = 'UTF-8';
  $mail->Host = 'smtp.gmail.com';
  $mail->Port = 587;
  $mail->SMTPAuth = true;
  $mail->Username = 'naoresponda@odia.com.br';
  $mail->Password = 'Ejesa2018';
  $mail->SMTPSecure = 'tls';
  $mail->From = 'naoresponda@odia.com.br';
  $mail->AddAddress('faleconosco@odia.com.br');
  $mail->AddBCC('testedigital@odia.com.br','');
  $mail->IsHTML(true);

  switch ($feedback) {
    case 'cancelar-assinatura':
      $auxFeedback = 'Quero cancelar minha assinatura';
      break;
    case 'cobranca-indevida':
      $auxFeedback = 'Problemas com cobranças';
      break;
    default:
      $auxFeedback = 'Geral';
      break;   
  }

  switch ($_SESSION['productId'])
  {
    case '000001':
      $mail->addAttachment(dirname(__FILE__)."/img/odia.png", 'odia' ,'odia.png');
      $mail->FromName = 'Jornal O Dia';
      $color = '#DA251D';
      $mail->Subject = ' Cancelamento - DASHBOARD O DIA IMPRESSO';
      $mail->Body = '
       <html>
       <body>
       <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#ffffff">
       <thead style="position:relative;padding:1% 7%;width:100%;background:#f3db33;border-bottom:1px solid #cebb35;">
       <th style="width:100%">
       <img style="max-width: 100px; padding: 10px;" src="cid:odia" />
       </th>
       </thead>
       <tbody style="display: block;">
       <tr>
       <td style="display: block; padding: 20px; font-family: Arial; letter-spacing: -1px;">
       <h1>Olá, Sac</h1>
       </td>
       <td style="display: block; padding: 20px; font-family: Arial;">
       <h4 style="">O assinante ' . $nome . ' está com problemas em sua assinatura.</h4>
       <p>Telefone: '.$telefone.'</p>
       <p>Plano: ' .$assinatura.'</p>
       <p>Assunto: '.$auxFeedback.'</p>
       <p>Mensagem: '.$msg.'</p>
       </td>
       <td style="display: block; padding: 20px; margin-top: 20px; text-align: center;">
       <a href="https://flip.odia.com.br/login.php" style="padding: 20px; padding: 18px; text-decoration: none; background:#174F82; border-radius: 3px; color: #FFFFFF; cursor: pointer; font-family: Arial,Helvetica,sans-serif; font-size: 18px;font-weight: bold;">Acessar agora!
       </a>
       </td>
       </tr>
       </tbody>
       </table>
       </body>
       </html>';
      break;

    case '000002':
      $mail->AddEmbeddedImage(dirname(__FILE__)."/img/odia.png", 'odia' ,'odia.png');
      $mail->FromName = 'Jornal O Dia';
      $color = '#DA251D';
      $mail->Subject = ' Cancelamento - DASHBOARD O DIA DIGITAL';
      $mail->Body = '
       <html>
       <body>
       <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#ffffff">
       <thead style="position:relative;padding:1% 7%;width:100%;background:#f3db33;border-bottom:1px solid #cebb35;">
       <th style="width:100%">
       <img style="max-width: 100px; padding: 10px;" src="cid:odia" />
       </th>
       </thead>
       <tbody style="display: block;">
       <tr>
       <td style="display: block; padding: 20px; font-family: Arial; letter-spacing: -1px;">
       <h1>Olá, Sac</h1>
       </td>
       <td style="display: block; padding: 20px; font-family: Arial;">
       <h4 style="">O assinante ' . $nome . ' está com problemas em sua assinatura.</h4>
       <p>Telefone: '.$telefone.'</p>
       <p>Plano: ' .$assinatura.'</p>
       <p>Assunto: '.$auxFeedback.'</p>
       <p>Mensagem: '.$msg.'</p>
       </td>
       <td style="display: block; padding: 20px; margin-top: 20px; text-align: center;">
       <a href="https://flip.odia.com.br/login.php" style="padding: 20px; padding: 18px; text-decoration: none; background:#174F82; border-radius: 3px; color: #FFFFFF; cursor: pointer; font-family: Arial,Helvetica,sans-serif; font-size: 18px;font-weight: bold;">Acessar agora!
       </a>
       </td>
       </tr>
       </tbody>
       </table>
       </body>
       </html>';
      break;

    case '000004':
      $mail->AddEmbeddedImage(dirname(__FILE__)."/img/mh.png", 'mh' ,'mh.png');
      $mail->FromName = 'Jornal Meia Hora';
      $color = '#DA251D';
      $mail->Subject = ' Cancelamento - DASHBOARD MEIA HORA DIGITAL';
      $mail->Body = '
        <html>
        <body>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#ffffff">
        <thead style="position:relative;padding:1% 7%;width:100%;background:#da251d;border-bottom:1px solid #000;">
        <th style="width:100%">
        <img style="max-width: 100px; padding: 10px;" src="cid:mh" />
        </th>
        </thead>
        <tbody style="display: block;">
        <tr>
        <td style="display: block; padding: 20px; font-family: Arial; letter-spacing: -1px;"><h1>Olá,'. $nome .'</h1></td>
        <td style="display: block; padding: 20px; font-family: Arial;">
        <h4 style="">O assinante ' . $nome . ' está com problemas em sua assinatura.</h4>
        <p>Telefone: '.$telefone.'</p>
        <p>Plano: ' .$assinatura.'</p>
        <p>Assunto: '.$auxFeedback.'</p>
        <p>Mensagem: '.$msg.'</p>
        </td>
        <td style="display: block; padding: 20px; margin-top: 20px; text-align: center;">
        <a href="https://flip.meiahora.com/login.php" target="_blank" style="padding: 20px; padding: 18px; text-decoration: none; background:#da251d; border-radius: 3px; color: #FFFFFF; cursor: pointer; font-family: Arial,Helvetica,sans-serif; font-size: 18px;font-weight: bold;">Acessar agora!
        </a>
        </td>
        </tr>
        </tbody>
        </table>
        </body>
        </html>';
      break;
  }
  
  return $mail->Send();
}

if (isset($_POST['btnCancelar'])) {
  
  $dissim = new Dissim();

  $result = $dissim->getContrato($_SESSION['email']);
  $contrato = json_decode($result);

  $dados = $dissim->getDados($_SESSION['email']);
  $dad = json_decode($dados);  

  $nome = $contrato[0]->NomeAssinante;
  $assinatura = $contrato[0]->NomeProduto;
  $telefone = '('.$dad[0]->DDD1.')'. " ".$dad[0]->Telefone1;


  if (empty($_POST['feedback'] && $_POST['msg'])) {

   echo 'Preencha todos os campos!';
   exit();

  } else {
    if (isset($_POST['feedback']) && !empty($_POST['feedback'])) {
      $feedback = $_POST['feedback'];
    }

    if (isset($_POST['msg']) && !empty($_POST['msg'])) {
      $msg = $_POST['msg'];
    }
  
    enviaEmail($_SESSION['productId'], $contrato[0]->NomeAssinante, $feedback, $msg, $telefone, $assinatura);
  
    echo 'Em breve entraremos em contato.';
  }
}