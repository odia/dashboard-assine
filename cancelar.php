<?php require 'header.php'; ?>
<style type="text/css">
  body, html{
    font-weight: 200;
    font-family: Circular, Helvetica, Arial, sans-serif;
  }

  .central{
    margin: 2% auto !important; 
    padding: 5% !important;  
    background: rgba(255, 255, 255, 0.5) !important;
    border-top: 20px solid rgba(237, 229, 92, 0.9);
  }

  .feedback{
    margin-bottom: 15px !important; 
  }

  .font3{
    padding: 0 0 0 0;
  }

  .div_cancela{
    margin: 0 auto;
  }

  .div_cancela2{
    margin: 1% 0 0 0
  }

  .div_cancela3{
    margin: 10px 0;
  }

  .div_cancela4{
    margin: 1% 0 0 0;
  }

  .font{ 
    font-weight: 400;
    line-height: 1.5;
  }

</style>

<main role="main" class="col-md-12 ml-sm-auto col-lg-12 px-5 font">
 <div class="col-md-8 order-md-3 div_cancela ">
   <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-2 pb-2 mb-3">
     <h3 class="text-left font3" ><span data-feather="users" style=""></span> Fale conosco</h3>
     <div class="btn-toolbar mb-2 mb-md-0">
      <div class="btn-group mr-2"></div>
    </div>
  </div> 
  <div class="col-md-12 order-md-3 central">
    <form method="POST" action="<?=$_ENV["APP_URL"]?>/cancelamento.php">
      <div class="row">
        <div class="col-md-12 mb-3 div_cancela2">
          <h6 class="feedback">Envie sua dúvida, sugestão ou reclamação. Entraremos em contato o mais breve possível!</h6>
          <div class="input-group" id="some">
            <select class="custom-select" id="feedback" name="feedback" aria-label="Example select with button ">
              <option value="" selected>Escolha uma opção</option>
              <option value="cancelar-assinatura">Quero cancelar minha assinatura</option>
	            <option value="cobranca-indevida">Problemas com cobranças</option>
            </select>
          </div>
          <div class="mb-3 div_cancela4">
            <label class="titulo">O que está ocorrendo?</label>
            <textarea type="textarea" rows="5" class="form-control" name="msg" id="msg" placeholder="Deixe sua mensagem aqui" required=""></textarea>
          </div>
          <hr class="mb-4">
          <button class="btn btn-light btn-lg btn-block">
            <a href="<?=$_ENV["APP_URL"]?>">Cancelar</a>
          </button>
          <button class="btn btn-success btn-lg btn-block" type="button" name="btnCancelar" id="btnCancelar" value="Enviar">Enviar</button>
        </div>
      </div>
    </form>
  </div>
</main>

<?php require 'footer.php'; ?> 