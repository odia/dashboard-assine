<?php
if (!isset($_SESSION)) {
    session_start();    
}

function geraSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false) {
    $lmin = 'abcdefghijklmnopqrstuvwxyz';
    $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $num = '1234567890';
    $simb = '!@#$%*-';
    $retorno = '';
    $caracteres = '';
    $caracteres .= $lmin;
    
    if ($maiusculas) $caracteres .= $lmai;
    if ($numeros) $caracteres .= $num;
    if ($simbolos) $caracteres .= $simb;
    $len = strlen($caracteres);
    
    for ($n = 1; $n <= $tamanho; $n++) {
        $rand = mt_rand(1, $len);
        $retorno .= $caracteres[$rand-1];
    }

    return $retorno;
}

require 'vendor/autoload.php';

use App\Controllers\Dissim;
use PHPMailer\PHPMailer\PHPMailer;

$dissim = new Dissim;

if (isset($_POST['btnEsqueci']) || (isset($_POST['cadastrar']))) { 

    if ($_POST['esqueci']) {
        $emailEsqueci = (isset($_POST['esqueci']) ? $_POST['esqueci']: '');
    } else {
        $emailEsqueci = (isset($_POST['emailAcesso']) ? $_POST['emailAcesso'] :'');
    }

    if (empty($emailEsqueci)) {
        echo 'Preencha o campo de e-mail!';
        exit();
    }

    $resultado = $dissim->getContrato($emailEsqueci);
    $contrato = json_decode($resultado);
    $plano = $contrato[0]->NomeProduto;

    if ($plano == 'O DIA DIGITAL') {
        $productId = '000002';
    } else {
        $productId = '000004';
    }

    if (isset($emailEsqueci)) { 
        $result = $dissim->AcessoDigital($emailEsqueci, $productId);
        
        if ($result == 'INATIVO') {
            echo 'E-mail não encontrado.';
        } else {
            $pass = geraSenha(8, true, true, true);
            $senha = trim($pass);
            $result = $dissim->GerarNovaSenha($emailEsqueci,$senha);

            if ($result == 'EMAIL NAO CADASTRADO' || $result == 'ERRO') {
                echo 'E-mail não cadastrado';
            } else { 
                $mail = new PHPMailer;
                $mail->IsSMTP();
                $mail->CharSet = 'UTF-8';
                $mail->Host = 'smtp.gmail.com';
                $mail->Port = 587;
                $mail->SMTPAuth = true; 
                $mail->Username = 'naoresponda@odia.com.br';
                $mail->Password = 'Ejesa2018';
                $mail->SMTPSecure = 'tls';
                $mail->From = 'naoresponda@odia.com.br';
                $mail->AddAddress($emailEsqueci, '');
                $mail->IsHTML(true);

                if($plano == 'O DIA DIGITAL') {
                    $mail->AddEmbeddedImage(dirname(__FILE__)."/img/odia.png", "odia", "odia.png");
                    $mail->FromName = 'Jornal O Dia';
                    $color = '#FEDD02';
                } else {
                    $mail->AddEmbeddedImage(dirname(__FILE__)."/img/mh.png", "mh", "meiahora.png");
                    $mail->FromName = 'Jornal Meia Hora';
                    $color = '#DA251D';
                }

                if ($plano == 'O DIA DIGITAL') {
                    $mail->Subject = 'Recuperar senha - DASHBOARD O DIA';
                    $mail->Body = '<html>
                    <body>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#ffffff">
                    <thead style="position:relative;padding:1% 7%;width:100%;background:#f3db33;border-bottom:1px solid #cebb35;">
                    <th style="width:100%">
                    <img style="max-width: 100px; padding: 10px;" src="cid:odia" />
                    </th>
                    </thead>
                    <tbody style="display: block;">
                    <center>
                    <tr>
                    <td style="display: block; padding: 20px; font-family: Arial; letter-spacing: -1px;"><h1>Esqueci minha senha!</h1></td>
                    <td style="display: block; padding: 20px; font-family: Arial;">
                    Você acaba de solicitar uma nova senha através do nosso dashboard <b>centraldoassinante.odia.com.br/</b>.
                    <br>Sua nova senha é: <b>'.$senha.'</b><br><br>
                    Clique no botão abaixo para acessar com a sua nova senha.
                    </td>
                    <td style="display: block; padding: 20px; margin-top: 20px; text-align: center;">
                    <a href="http://flip.odia.com.br/login.php" style="padding: 20px; padding: 18px; text-decoration: none; background:#174F82; border-radius: 3px; color: #FFFFFF; cursor: pointer; font-family: Arial,Helvetica,sans-serif; font-size: 18px;font-weight: bold;">Acessar agora!
                    </a>
                    </td>
                    </tr>
                    </center>
                    </tbody>
                    </table>
                    </body>
                    </html>';
                } else {
                    $mail->Subject = 'Recuperar senha - DASHBOARD MEIA HORA';
                    $mail->Body    = '
                    <html>
                    <body>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#ffffff">
                    <thead style="position:relative;padding:1% 7%;width:100%;background:#da251d;border-bottom:1px solid #000;">
                    <th style="width:100%">
                    <img style="max-width: 100px; padding: 10px;" src="cid:mh" />
                    </th>
                    </thead>
                    <tbody style="display: block;">
                    <tr>
                    <td style="display: block; padding: 20px; font-family: Arial; letter-spacing: -1px;"><h1>Esqueci minha senha!</h1></td>
                    <td style="display: block; padding: 20px; font-family: Arial;">
                    Você acaba de solicitar uma nova senha através do nosso dashboard <b>centraldoassinante.odia.com.br/</b>.
                    <br>Sua nova senha é: <b>'.$senha.'</b><br><br>
                    Clique no botão abaixo para acessar com a sua nova senha.
                    </td>
                    <td style="display: block; padding: 20px; margin-top: 20px; text-align: center;">
                    <a href="http://flip.meiahora.com/login.php" target="_blank" style="padding: 20px; padding: 18px; text-decoration: none; background:#da251d; border-radius: 3px; color: #FFFFFF; cursor: pointer; font-family: Arial,Helvetica,sans-serif; font-size: 18px;font-weight: bold;">Acessar agora!
                    </a>
                    </td>
                    </tr>
                    </tbody>
                    </table>
                    </body>
                    </html>';
                }
        
                $mail->AltBody = 'Voce solicitou uma nova senha através do nosso dashboard. Sua nova senha:'.$senha;

                if (!$mail->Send()) {
                    echo 'Houve um erro, coloque corretamente seu e-mail!';
                } else {
                    echo "E-mail enviado com sucesso!!!";
                }
            }
        }
    }
}