<?php
$pagina = 'Esqueci minha senha';
include('head.php');
?>
<section>
	<div class="row">
		<div class="col-md-12">
			<h1>Redefinição de senha</h1>
			<div class="col-md-6">
				<p>Insira o e-mail usado no momento do cadastro. Mandaremos um e-mail com sua nova senha de acesso.</p>
			</div>
			<form>
				<div class="form-group">
					<label>
						<strong>Endereço de e-mail</strong>
					</label>
					<div class="col-md-6">
						<input type="email" class="form-control" id="esqueci" name="esqueci" placeholder="E-mail" required>
					</div>
				</div>
				<div class="col-md-4">
					<button type="button" name="btnEsqueci" id="btnEsqueci" class="btn btn-outline-success btn-lg btn-block">Redefinir</button>
				</div>
			</form>
		</div>
	</div>
</section>

<?php include('footer.php'); ?>