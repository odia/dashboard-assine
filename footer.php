<script src="<?=$_ENV["APP_URL"] ?>/js/jquery.js"></script>
<script src="<?=$_ENV["APP_URL"] ?>/js/app.js?version=<?=time()?>"></script>
<script src="<?=$_ENV["APP_URL"] ?>/js/bootstrap.min.js"></script>
<script src="<?=$_ENV["APP_URL"] ?>/js/popper.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
  feather.replace()
  
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });
</script>
</body>
</html>