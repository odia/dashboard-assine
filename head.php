<?php
require 'vendor/autoload.php';
	
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();
?>
<!doctype html>
<html lang="pt-br">
<head>
	<title><?=$pagina ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="<?=$_ENV["APP_URL"]?>/css/bootstrap-grid.min.css">
	<link rel="stylesheet" href="<?=$_ENV["APP_URL"]?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=$_ENV["APP_URL"]?>/css/dashboard.css">
	<link rel="stylesheet" href="<?=$_ENV["APP_URL"]?>/css/first.css">
	<link rel="stylesheet" href="<?=$_ENV["APP_URL"]?>/css/signin.css">

	<!-- CUSTOMS LARA -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/headerfooter.css">

</head>