<?php 
require 'verifica.php';
require 'vendor/autoload.php';
use App\Controllers\Dissim;

$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();
?>
<!doctype html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Dashboard - Assinatura</title>
  <link rel="stylesheet" href="<?=$_ENV["APP_URL"]?>/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=$_ENV["APP_URL"]?>/css/dashboard.css">
  <link rel="stylesheet" href="<?=$_ENV["APP_URL"]?>/css/form-validation.css">
  <link rel="stylesheet" href="<?=$_ENV["APP_URL"]?>/css/sidebar.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <style>
    body{
      background: rgba(220, 224, 232, 0.2) !important;
      font-weight: 200 !important;
      line-height: none !important;
    }

    .border_menu {
      border-bottom: 1px solid rgba(234, 237, 242, 0.2);
      color: rgba(252, 253, 255, 0.8) !important;
      padding: 0.8rem 1rem;
    }

    .user-photo{
      width: 10rem; 
      height: 4rem;
      border: 1px solid #222326; 
      border-radius: 50%; 
      width: 64px; 
      height: 64px; 
      color: #222326 !important;
    }

    .borda-menu{
      border-left: 2px solid #f3db33 !important;
    }

    @media (max-width: 576px) { 
      body{
        font-size: 13px;
      }
      html{
        font-size: 13px;
      }
    }
</style>
</head> 
<body>
  <div class="d-flex" id="wrapper">
    <?php
      $dissim = new Dissim();
      $result = $dissim->getContrato($_SESSION['email']);
      $contrato = json_decode($result);

      switch ($_SESSION['productId']) {

        case '000001':
          echo '<div class="bg-light border-right" id="sidebar-wrapper">
          <div class="sidebar-heading"></div>
          <center> <h5 style="vertical-align: 0; height:20px; transform: translate(0%, -50%); top: 50%; left: 50%;"> O Dia Impresso </h5>';
        break;

        case '000002':
          echo '<div class="bg-light border-right" id="sidebar-wrapper">
          <div class="sidebar-heading"></div>
          <center>
          <img src="img/odia.svg" style="vertical-align: 0; height:20px; transform: translate(0%, -50%); top: 50%; left: 50%;">';
        break;

        case '000004':
          echo '<div class="bg-dark border-right" id="sidebar-wrapper">
          <div class="sidebar-heading"></div>
          <center>
          <img src="img/mh.svg" style="vertical-align: 0; height:20px; transform: translate(0%, -50%); top: 50%; left: 50%;">';
        break;

        default:
          echo 'Produto não encontrado';
        break;
    }
    ?>
  <div class="list-group list-group-flush">

    <a href="<?=$_ENV["APP_URL"]?>/index.php" class="list-group-item list-group-item-action bg-light">
      <span data-feather="home"></span> 
      Meus dados
    </a>

    <a href="<?=$_ENV["APP_URL"]?>/mudar.php" class="list-group-item list-group-item-action bg-light">
      <span data-feather="lock"></span>
      Mudar Senha
    </a>

    <a href="<?=$_ENV["APP_URL"]?>/cancelar.php" class="list-group-item list-group-item-action bg-light">
      <span data-feather="users"></span>
      Fale Conosco
    </a>

    <a href="<?=$_ENV["APP_URL"]?>/logout.php" class="list-group-item list-group-item-action bg-light">
      <span data-feather="x-square"></span> 
      Sair
    </a>
  </div>
</div>

<div id="page-content-wrapper">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark border-bottom">
    <button class="btn" id="menu-toggle"><span class="navbar-toggler-icon"></span></button>
  </nav>