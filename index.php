<?php
require 'header.php';
?>
<style type="text/css">
    .font {
        font-family: Circular, Helvetica, Arial, sans-serif; 
        font-weight: 400;
        line-height: 1.5;
    }
</style>
<main role="main" class="col-md-12 ml-sm-auto col-lg-12 px-5">
    <div class="col-md-8 order-md-3" style="margin: 0 auto;">
     <div class="d-flex justify-content-between flex-wrap flex-md-nowrap pt-1 pb-1 mb-3">
        <h3 style="padding: 0 0 0 0;"> <span data-feather="home"></span> Meus dados </h3>
    </div> 
    <div class="col-md-12 order-md-3" style="float: left !important; position: relative !important;">
     <hr style="width: 100%; margin-top: 0;">
     <div class="shadow-lg mb-5 rounded">
        <div class="card mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 text-primary">Perfil</h6>
            </div>
            <div class="card-body">
                <div>
                    <h6 class="text-secondary">Nome do usuario:</h6>
                    <p class="font"><?php echo $contrato[0]->NomeAssinante; ?></p>
                </div>
                <div class="camposAssinante">
                    <h6 class="text-secondary">E-mail:</h6>
                    <p class="font"><?php echo $_SESSION['email']; ?> </p>
                </div>
                <div class="camposAssinante">
                    <h6 class="text-secondary">Cep:</h6>
                    <p class="font">  
                        <?php
                        $cod = $contrato[0]->CodAssinante;
                        $status = $contrato[0]->Contrato;
                        $resultado = $dissim->getEndereco($cod, $status);
                        $endereco = json_decode($resultado);

                        echo $endereco[0]->Cep;
                        ?>
                    </p>
                </div>
                <div class="camposAssinante">
                    <h6 class="text-secondary">Endereço:</h6>
                    <p class="font">
                        <?php
                        $cod = $contrato[0]->CodAssinante;
                        $status = $contrato[0]->Contrato;
                        $resultado = $dissim->getEndereco($cod, $status);
                        $endereco = json_decode($resultado);
                        echo ($endereco[0]->TipoLogradouro) . " " . $endereco[0]->Logradouro ." N°". $endereco[0]->logradouroNumero ;
                        ?>
                    </p>
                </div>
                <div class="camposAssinante">
                    <h6 class="text-secondary">Telefone:</h6>
                    <p class="font">
                       <?php
                       $dados = $dissim->getDados($_SESSION['email']);
                       $dad = json_decode($dados);  
                       echo '('.$dad[0]->DDD1.')'. " ".$dad[0]->Telefone1;
                       ?>
                   </p>
               </div>
               <div class="camposAssinante">
                <h6 class="text-secondary">Plano de assinatura:</h6>
                <p class="font"> <?php echo $contrato[0]->NomeProduto;?> </p>
            </div>
            <div class="camposAssinante">
                <h6 class="text-secondary">Codigo do Assinante:</h6>
                <p class="font"><?php echo $contrato[0]->CodAssinante;?> </p>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</main>

<?php require 'footer.php'; ?> 