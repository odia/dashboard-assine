$(document).ready(function(){ 
	const Toast = Swal.mixin({
		toast: true,
		position: 'top',
		showConfirmButton: false,
		timer: 3000
	});
	
	//ESQUECI MINHA SENHA
	function retornaNovaSenha(data){
		if (data == 'E-mail enviado com sucesso!!!') {
			$("#mudar-sucesso").html(Toast.fire({
				type: 'success',
				title: data
			}));
		} else {
			$("#mudar-error").html(Toast.fire({
				type: 'error',
				title: data
			}));
		}
	}

	//CADASTRAR
	function primeiroAcesso(data){
		if (data == 'E-mail enviado com sucesso!!!') {
			$("#mudar-sucesso").html(Toast.fire({
				type: 'success',
				title: data
			}));
		} else {
			$("#mudar-error").html(Toast.fire({
				type: 'error',
				title: data
			}));
		}
	}

	//NOVA SENHA
	function mudarSenha(data){
		if (data == 'SENHA TROCADA COM SUCESSO') {
			$("#mudar-sucesso").html(Toast.fire({
				type: 'success',
				title: data
			}));
		} else {
			$("#mudar-error").html(Toast.fire({
				type: 'error',
				title: data
			}));
		}
	}

	//AUTENTICAÇÃO
	function Autenticar(data){
		if (data == 'Autenticado') {
			$("#mudar-sucesso").html(Toast.fire({
				type: 'success',
				title: data
			}));
		} else {
			$("#mudar-error").html(Toast.fire({
				type: 'error',
				title: data
			}));
		}
	}

	//Cancelamento
	function Cancelamento(data){
		if (data == 'Em breve entraremos em contato.') {
			$("#mudar-sucesso").html(Toast.fire({
				type: 'success',
				title: data
			}));
		} else {
			$("#mudar-error").html(Toast.fire({
				type: 'error',
				title: data
			}));
		}
	}

	// ESQUECI SENHA - apertando a tecla 'Enter'
	$("#esqueci").keydown(function( event ) {
		if ( event.which == 13 ) {
			event.preventDefault();	
			
			$("#btnEsqueci").html('Aguarde ...');
			$("#btnEsqueci").prop('disabled', true);

			$.ajax({
				type: "POST",
				data: { btnEsqueci: $("#btnEsqueci").val() , esqueci: $("#esqueci").val() },
				url: "esqueci-senha.php", 
				success: retornaNovaSenha,
			}).always(function(data){
				if ( data == 'E-mail enviado com sucesso!!!') {
					setTimeout(function(){
						window.location.assign("login.php");
					}, 2000);
				} else {
					setTimeout(function(){
						$("#btnEsqueci").html('Redefinir');
						$("#btnEsqueci").prop('disabled', false);
					}, 2000);
				}
			});	
		}
	});

	// ESQUECI SENHA - clicando no botão 'Redefinir'
	$("#btnEsqueci").click(function(){
		$(this).html('Aguarde ...');
		$(this).prop('disabled', true);
		
		$.ajax({
			type: "POST",
			data: { btnEsqueci: $(this).val() , esqueci: $("#esqueci").val() },
			url: "esqueci-senha.php", 
			success: retornaNovaSenha,
		}).always(function(data){
			if ( data == 'E-mail enviado com sucesso!!!') {
				setTimeout(function(){
					window.location.assign("login.php");
				}, 2000);
			} else {
				setTimeout(function(){
					$("#btnEsqueci").html('Enviar');
					$("#btnEsqueci").prop('disabled', false);
				}, 2000);
			}
		});
	});

	// Primeiro Acesso - apertando a tecla 'Enter'
	$("#emailAcesso").keydown(function(event){
		if(event.which == 13){
			event.preventDefault();

			var _email = $("#emailAcesso").val();

			if(_email == ""){
				primeiroAcesso('Por favor, informe seu email.');
			} else {
				$(this).html('Aguarde ...');
				$(this).prop('disabled', true);

				$.ajax({
					type: "POST",
					data: { cadastrar: $(this).val() , esqueci: _email },
					url: "esqueci-senha.php", 
					success: primeiroAcesso,
				}).always(function(data){
					if ( data == 'E-mail enviado com sucesso!!!') {
						setTimeout(function(){
							$("#cadastrar").html('Voltar');
							$("#cadastrar").prop('disabled', false);
						}, 2000);
					} else {
						setTimeout(function(){
							$("#cadastrar").html('Enviar');
							$("#cadastrar").prop('disabled', false);
						}, 2000);
					}
				});
			}
		}
	});

	// Primeiro Acesso - clicando no botão 'Cadastrar'
	$("#cadastrar").click(function(){
		$(this).html('Aguarde ...');
		$(this).prop('disabled', true);

		$.ajax({
			type: "POST",
			data: { cadastrar: $(this).val() , esqueci: $("#emailAcesso").val() },
			url: "esqueci-senha.php", 
			success: primeiroAcesso,
		}).always(function(data){
			if ( data == 'E-mail enviado com sucesso!!!') {
				setTimeout(function(){
					$("#cadastrar").html('Voltar');
					$("#cadastrar").prop('disabled', false);
				}, 2000);
			} else {
				setTimeout(function(){
					$("#cadastrar").html('Enviar');
					$("#cadastrar").prop('disabled', false);
				}, 2000);
			}
		});
	});

	//NOVA SENHA - apertando a tecla 'Enter'
	$("#novo").keydown(function(event){
		if ( event.which == 13 ) {
			event.preventDefault();

			var _senhaVelha = $("#anterior").val();
			var _senhaNova = $("#novo").val();

			if(_senhaVelha == '' || _senhaNova == ''){
				mudarSenha('Por favor, preencha todos os campos.');
			} else {

				$("#mudarSenha").html('Aguarde ...');
				$("#mudarSenha").prop('disabled', true);

				$.ajax({
					type:"POST",
					data: { mudarSenha: $("#mudarSenha").val(), novo: _senhaNova, anterior: _senhaVelha },
					url: "mudar-senha.php",
					success: mudarSenha,
				}).always(function(data){
					setTimeout(function(){
						$("#mudarSenha").prop('disabled', false);
						$("#mudarSenha").html('Definir nova senha');
					}, 2000);
				});
			}
		}	
	});

	//NOVA SENHA - clicando no botão 'Definir nova senha'
	$("#mudarSenha").click(function(){
		var _senhaVelha = $("#anterior").val();
		var _senhaNova = $("#novo").val();

		if(_senhaVelha == '' || _senhaNova == ''){
			mudarSenha('Por favor, preencha todos os campos.');
		} else {

			$("#mudarSenha").html('Aguarde ...');
			$("#mudarSenha").prop('disabled', true);

			$.ajax({
				type:"POST",
				data: { mudarSenha: $("#mudarSenha").val(), novo: _senhaNova, anterior: _senhaVelha },
				url: "mudar-senha.php",
				success: mudarSenha,
			}).always(function(data){
				setTimeout(function(){
					$("#mudarSenha").prop('disabled', false);
					$("#mudarSenha").html('Definir nova senha');
				}, 2000);
			});
		}
	});

	//AUTENTICAÇÃO
	$("#auth").click(function(){
		$(this).html('Aguarde ...');
		$(this).prop('disabled', true);

		$.ajax({
			type:"POST",
			data: { btn: $(this).val(), email: $("#email").val(), senha: $("#senha").val() },
			url: "auth.php",
			success: Autenticar,
		}).always(function(data) {
			if (data == 'Autenticado') {
				setTimeout(function(){
					window.location.assign("index.php");
				}, 3000);
			} else {
				setTimeout(function(){
					$("#auth").html('Entrar');
					$("#auth").prop('disabled', false);
				}, 3000);
			}
		})
	});

	//CANCELAMENTO
	$("#formGroupExampleInput").hide();
	$("#caixa").click(function(){
		setTimeout(function(){
			$("#formGroupExampleInput").show();
		}, 1000);
		$("#some").empty();
	});

	$("#btnCancelar").click(function(){
		$("#btnCancelar").html('Aguarde ...');
		$("#btnCancelar").prop('disabled', true);
		$.ajax({
			type:"POST",
			data: { btnCancelar: $("#btnCancelar").val(), feedback: $("#feedback").val(), msg: $("#msg").val()  },
			url: "cancelamento.php",
			success: Cancelamento,
		}).always(function(data){
			setTimeout(function(){
				$("#btnCancelar").prop('disabled', false);
				$("#btnCancelar").html('Definir nova senha');
			}, 3000);
		});
	});
});