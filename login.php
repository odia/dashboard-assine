<?php
if (isset($_SESSION['email']) && isset($_SESSION['senha']) && isset($_SESSION['productId'])) { 
  header('Location: index.php');
}

$pagina = 'Bem vindo ao Dashboard do Jornal O Dia';
include('head.php');
?>

<body style="background-color: #ECECEC; display: block;">

  <div class="background">
    <div class="container text-center">
      <h1 class="type-title"><a href="#">Central do Assinante <strong>O Dia e Meia Hora</strong></a></h1>
    </div>
  </div>

  <div class="container text-center">
    <div class="box-form">
      <div class="form-content">
        <h4>Central do Assinante</h4>

          <form class="form-signin" method="POST" action="<?=$_ENV["APP_URL"]?>/auth.php">
            <div class="container-fluid">
              <div class="form-group">
                <div class="form-row">
                  <div class="col-md-12">
                    <input class="form-control" type="email" id="email" name="email" placeholder="E-mail" />
                  </div>
                  <div class="col-md-12">
                    <input class="form-control" type="password" id="senha" name="senha" placeholder="Senha"/>
                  </div>
                  <div class="col-md-12">
                    <button type="button" name="btn" id="auth" class="btn btn-primary buttontype">ENTRAR</button>
                    <p class="pform"><a href="<?=$_ENV["APP_URL"]?>/esqueci.php">Esqueceu a senha?</a></p>
                    <p class="pform"><a href="<?=$_ENV["APP_URL"]?>/primeiro-acesso.php">Primeiro acesso?</a></p>
                  </div>
                </div>
              </div>
            </div>
          </form>
      </div>
    </div>
  </div>

  <div class="background" style="padding-bottom: 2%">
    <div class="container text-center">
      <div class="row">
        <div class="col-md-12">
          <h2>Contato</h2>
          <p>Em caso de dúvidas, entre em contato com a nossa Central de Atendimento ao Assinante.</p>
          <p>2º a 6º das 8 às 16 horas. Sábados, Domingos e Feriados das 9 às 14 horas</p>
        </div>
        <div class="col-md-6 txt-left">
          <h3>(21) 2222-8600</h3>
        </div>
        <div class="col-md-6 txt-right">
          <h3>assinaturaodia@odianet.com.br</h3>
        </div>
      </div>
    </div>
  </div>

<?php include('footer.php'); ?>
