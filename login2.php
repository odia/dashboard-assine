<?php
if (isset($_SESSION['email']) && isset($_SESSION['senha']) && isset($_SESSION['productId'])) { 
  header('Location: index.php');
}

$pagina = 'Bem vindo ao Dashboard do Jornal O Dia';
include('head.php');
?>
<body class="text-center">
  <form class="form-signin shadow-lg mb-12 rounded form" method="POST" action="<?=$_ENV["APP_URL"]?>/auth.php" >
    <h1 class="h3 mb-3 font-weight-normal">Central do Assinante</h1>
    <label class="sr-only">E-mail</label>
    <input type="email" id="email" name="email" class="form-control" placeholder="E-mail" required="">
    <label class="sr-only">Senha</label>
    <input type="password" id="senha" name="senha" class="form-control" placeholder="Senha" required="">
    <label>
      <a value="remember-me" href="<?=$_ENV["APP_URL"]?>/esqueci.php">Esqueci minha senha</a>
    </label>
    <div></div>
    <button class="btn btn-lg btn-primary btn-block" type="button" name="btn" id="auth">Entrar</button>
    <label style="margin-top: 10px;">
      <a value="remember-me" href="<?=$_ENV["APP_URL"]?>/primeiro-acesso.php">Primeiro acesso? Clique Aqui.</a>
    </label>
  </form>
</body>

<?php include('footer.php'); ?>