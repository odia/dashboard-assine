<?php
if (!isset($_SESSION)) { 
  session_start();
}

require 'src/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;

//conectando dissim
$client = new SoapClient(null, array(
  'location' => 'http://200.98.205.66/asw/WSDL/server.php',  
  'uri' => 'http://200.98.205.66/asw/WSDL/',                 
  'trace' => 1)
);

function enviaEmail($productId, $nome) {

  $mail = new PHPMailer;
  $mail->IsSMTP();
  $mail->CharSet = 'UTF-8';
  $mail->Host = 'smtp.gmail.com';
  $mail->Port = 587;
  $mail->SMTPAuth = true;
  $mail->Username = 'naoresponda@odia.com.br';
  $mail->Password = 'Ejesa2018';
  $mail->SMTPSecure = 'tls';
  $mail->From = 'naoresponda@odia.com.br';

  switch ($_SESSION['productId']) {

    case '000001':
    $mail->AddEmbeddedImage(dirname(__FILE__)."/img/odia.png", 'odia' ,'odia.png');
    $mail->FromName = 'Jornal o Dia';
    $color = '#FEDD02';
    break;

    case '000002':
    $mail->AddEmbeddedImage(dirname(__FILE__)."/img/odia.png", 'odia' ,'odia.png');
    $mail->FromName = 'Jornal o Dia';
    $color = '#FEDD02';
    break;

    case '000004':
    $mail->AddEmbeddedImage(dirname(__FILE__)."/img/mh.png", 'mh' ,'mh.png');
    $mail->FromName = 'Jornal Meia Hora';
    $color = '#DA251D';
    break;

    default:
    exit();
    break;
  }
  $mail->AddAddress($_SESSION['email'], '');
  $mail->IsHTML(true);

  switch ($_SESSION['productId']) {
    case '000001': 
    $mail->Subject = 'Alteracao de senha - DASHBOARD O DIA IMPRESSO';
    $mail->Body = '
    <html>
    <body>
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#ffffff">
    <thead style="position:relative;padding:1% 7%;width:100%;background:#f3db33;border-bottom:1px solid #cebb35;">
    <th style="width:100%">
    <img style="max-width: 100px; padding: 10px;" src="cid:odia" />
    </th>
    </thead>
    <tbody style="display: block;">
    <tr>
    <td style="display: block; padding: 20px; font-family: Arial; letter-spacing: -1px;"><h1>Olá,'. $nome .'</h1></td>
    <td style="display: block; padding: 20px; font-family: Arial;">
    <h4>Acabamos de receber um solicitacao para alterar sua senha via nosso <b><em>dashboard</em></b>.</h4>
    <h4>Clique no botao abaixo para acessar com a sua nova senha.</h4>
    </td>
    <td style="display: block; padding: 20px; margin-top: 20px; text-align: center;">
    <a href="http://flip.odia.com.br/login.php" style="padding: 20px; padding: 18px; text-decoration: none; background:#174F82; border-radius: 3px; color: #FFFFFF; cursor: pointer; font-family: Arial,Helvetica,sans-serif; font-size: 18px;font-weight: bold;">Acessar agora!
    </a>
    </td>
    </tr>
    </tbody>
    </table>
    </body>
    </html>';
    break;

    case '000002':
    $mail->Subject = 'Alteracao de senha - DASHBOARD O DIA';
    $mail->Body = '
    <html>
    <body>
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#ffffff">
    <thead style="position:relative;padding:1% 7%;width:100%;background:#f3db33;border-bottom:1px solid #cebb35;">
    <th style="width:100%">
    <img style="max-width: 100px; padding: 10px;" src="cid:odia" />
    </th>
    </thead>
    <tbody style="display: block;">
    <tr>
    <td style="display: block; padding: 20px; font-family: Arial; letter-spacing: -1px;"><h1>Olá,'. $nome .'</h1></td>
    <td style="display: block; padding: 20px; font-family: Arial;">
    <h4>Acabamos de receber um solicitacao para alterar sua senha via nosso <b><em>dashboard</em></b>.</h4>
    <h4>Clique no botao abaixo para acessar com a sua nova senha.</h4>
    </td>
    <td style="display: block; padding: 20px; margin-top: 20px; text-align: center;">
    <a href="http://flip.odia.com.br/login.php" style="padding: 20px; padding: 18px; text-decoration: none; background:#174F82; border-radius: 3px; color: #FFFFFF; cursor: pointer; font-family: Arial,Helvetica,sans-serif; font-size: 18px;font-weight: bold;">Acessar agora!
    </a>
    </td>
    </tr>
    </tbody>
    </table>
    </body>
    </html>';
    break;

    case '000004':
    $mail->Subject = 'Recuperar senha - DASHBOARD MEIA HORA';
    $mail->Body    = '
    <html>
    <body>
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#ffffff">
    <thead style="position:relative;padding:1% 7%;width:100%;background:#da251d;border-bottom:1px solid #000;">
    <th style="width:100%">
    <img style="max-width: 100px; padding: 10px;" src="cid:mh" />
    </th>
    </thead>
    <tbody style="display: block;">
    <tr>
    <td style="display: block; padding: 20px; font-family: Arial; letter-spacing: -1px;"><h1>Olá,'. $nome .'</h1></td>
    <td style="display: block; padding: 20px; font-family: Arial;">
    <h4>Acabamos de receber um solicitacao para alterar sua senha via nosso <b><em>dashboard</em></b>.</h4>
    <h4>Clique no botao abaixo para acessar com a sua nova senha.</h4>
    </td>
    <td style="display: block; padding: 20px; margin-top: 20px; text-align: center;">
    <a href="http://flip.meiahora.com/login.php" target="_blank" style="padding: 20px; padding: 18px; text-decoration: none; background:#da251d; border-radius: 3px; color: #FFFFFF; cursor: pointer; font-family: Arial,Helvetica,sans-serif; font-size: 18px;font-weight: bold;">Acessar agora!
    </a>
    </td>
    </tr>
    </tbody>
    </table>
    </body>
    </html>';
    break;

    default:
    echo "Produto não identificado.";
    break;

    if(!$mail->Send()) {
      echo 'Não foi possível enviar a mensagem.<br>';
      echo 'Erro: ' . $mail->ErrorInfo;
    }else{
      $msg = "E-mail enviado com sucesso.";
      echo $msg;
    }
  }
  return $mail->Send();
}

if (isset($_POST['mudarSenha'])) {

  include("dissim.php");
  $dissim = new Dissim(); 

  $result = $dissim->getContrato($_SESSION['email']);
  $contrato = json_decode($result);

  if (empty($_POST['novo'] && $_POST['anterior'])) {
    echo "Preencha todos os campos!";
    exit();
  } else {

    if (isset($_POST['novo']) && !empty($_POST['novo'])) {
      $novaSenha = $_POST['novo'];
    }

    if (isset($_POST['anterior']) && !empty($_POST['anterior'])) {
      $senhaAnterior = $_POST['anterior'];
    }
  }

  $mudar = $dissim->trocarSenha($_SESSION['email'], $senhaAnterior, $novaSenha);

  echo $mudar;

  if ($mudar == 'SENHA TROCADA COM SUCESSO') {
    enviaEmail($_SESSION['productId'], $contrato[0]->NomeAssinante);
  }

}else{
  echo "Nenhuma requisição foi enviada!";
}