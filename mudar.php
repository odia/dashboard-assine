<?php
include('header.php');
?>

<style type="text/css">

  main{
    margin: 0 0;
  }

  div{
    margin: 0 auto;
  }

  label{
    color: rgb(190, 194, 188);
    font-size: 16px;
    font-family: 'Open Sans', sans-serif;
  }

  .tipo_titulo{
    font-family: 'Open Sans', sans-serif;
  }

  .font{
    font-family: Circular, Helvetica, Arial, sans-serif; 
    font-weight: 400;
    line-height: 1.5;
  }

</style>

<main role="main" class="col-md-12 ml-sm-auto col-lg-12 px-5 font">

  <div class="col-md-8 order-md-3">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-2 pb-2 mb-3 border-bottom">
      <h3 class="tipo_titulo"><span data-feather="lock"></span> Mudar senha</h3>
    </div> 
    <form>
      <div class="row">
        <div class="col-md-12 mb-3">
          <label>Senha atual </label>
          <input type="password" class="form-control" name="anterior" id="anterior" required>
          <div class="mb-3">
            <label>Nova senha <span class="text-muted"></span></label>
            <input type="password" class="form-control" name="novo" id="novo" required>
          </div>
          <hr class="mb-4">
          <button class="btn btn-light btn-lg btn-block"><a href="http://<?php echo $_SERVER['SERVER_NAME'];?>/">Cancelar</a></button>
          <button class="btn btn-success btn-lg btn-block" type="button" name="mudarSenha" id="mudarSenha" value="novasenha">Definir nova senha</button>
        </div>
      </div>
    </form>
  </div>
</main>

<?php include('footer.php'); ?> 