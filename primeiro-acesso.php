<?php
$pagina = 'Primeiro Acesso';
include('head.php');
?>
<section>
	<div class="row">
		<div class="col-md-12">
			<h1><strong>Primeiro acesso</strong></h1>
			<div class="col-md-6">
				<p>Insira o e-mail usado no momento do cadastro. Mandaremos um e-mail com sua nova senha de acesso.</p>
			</div>
			<form>
				<div class="form-group">
					<label>
						<strong>Endereço de e-mail</strong>
					</label>
					<div class="col-md-6">
						<input type="email" class="form-control" id="emailAcesso" name="emailAcesso" placeholder="E-mail" required>
					</div>
				</div>
				<div class="col-md-4">
					<button class="btn btn-outline-light btn-lg btn-block">
						<a href="<?=$_ENV["APP_URL"]?>">Voltar</a>
					</button>
					<button type="button" name="cadastrar" id="cadastrar" class="btn btn-outline-success btn-lg btn-block">
						Cadastrar
					</button>
				</div>
			</form>
		</div>
	</div>
</section>

<?php include('footer.php'); ?>