<?php
namespace App\Controllers;

class Dissim {

	private $client = null;
	private $senhaSeguranca = "D15SiM#@";
	
	public function __construct() {
		$this->client = $this->getClient();
	}
	/**
	 * Método que realiza a conexão via protocolo SOAP com o web service do DISSIM
	 * 
	 * @return resource
	 */
	private function getClient() {
		return new \SoapClient(null, array(
			'location' => 'http://200.98.205.66/asw/WSDL/server.php',  
			'uri' => 'http://200.98.205.66/asw/WSDL/',                 
			'trace' => 1
		));
	}

	/**
	 * Lista todos os dados de um determinado contrato
	 * @param string $email
	 * 
	 * @return object dados do contrato
	 */
	public function getContrato($email) {
		$contrato = $this->client->WSListaContratos($email, $this->senhaSeguranca);
		return $contrato;
	}

	/**
	 * Lista os dados de endereço de um assinante
	 * @param string $CodigoAssinante
	 * @param string $contrato
	 * 
	 * @return object dados de endereço (e.g. logradouro, número, complemento, bairro, cidade)
	 */
	public function getEndereco($CodigoAssinante, $contrato) {
		$endereco = $this->client->WSListaEndereco($CodigoAssinante, $contrato, $this->senhaSeguranca);
		return $endereco;
	}
	
	/**
	 * Lista todos os dados de um determinado assinante
	 * @param string $email
	 * 
	 * @return object dados do assinante
	 */
	public function getDados($email) {
		$dados = $this->client->WSDadosAssinante($email, $this->senhaSeguranca);
		return $dados;
	}

	/**
	 * Método usado para autenticar o assinante. Caso o 
	 * @param string $email email do assinante
	 * @param string $senha senha de acesso
	 * @param string $productId código do produto assinado pelo usuário
	 * 
	 * @return object dados do usuário (e.g. se está ativo ou não)
	 */
	public function WSAcessoAssinante($email, $senha, $productId) {
		return $this->client->WSAcessoAssinante($email, $senha, $productId, $this->senhaSeguranca);
	}

	/**
	 * Método usado para atualizar a senha do assinante junto ao DISSIM
	 * @param string $email
	 * @param string $senhaAtual
	 * @param string $senhaNova
	 * 
	 * @return object status da operação (se a senha foi trocada ou não)
	 */
	public function trocarSenha($email, $senhaAtual, $senhaNova) {
		return $this->client->WSTrocaSenhaAssinante($email, $senhaNova, $senhaAtual, $this->senhaSeguranca); 
	}	

	/**
	 * Método usado para atualizar os dados de um assinante no DISSIM.
	 * @param string $assinante código do assinante no DISSIM
	 * @param object objeto JSON com os dados a serem atualizados
	 * 
	 * @return object status da operação (se o registro do assinante foi alterado ou não)
	 */	
	public function WSDadosAssinanteAltera($assinante, $array_json) {
		return $this->client->WSDadosAssinanteAltera($assinante, $array_json, $this->senhaSeguranca);
	}

	/**
	 * Método usado para consultar o status de uma assinatura digital.
	 * @param string $emailEsqueci
	 * @param string $productId código do produto assinado
	 * 
	 * @return string mensagem de retorno com o status da assinatura
	 */
	public function AcessoDigital($emailEsqueci, $productId){
		return $this->client->WSAcessoEmailDigital($emailEsqueci, $productId,$this->senhaSeguranca);
		
	}

	/**
	 * Método usado para gerar uma nova senha para um assinante.
	 * @param string $email
	 * @param string $senha
	 * 
	 * @return string mensagem de retorno com o status da operação
	 */
	public function GerarNovaSenha($email,$senha){
		return $this->client->WSNovaSenhaAssinante($email,$senha,$this->senhaSeguranca);
	}
}
?>