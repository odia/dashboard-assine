<?php

function destroiSessao(){
	session_destroy();
	header("Location: login.php"); 
	exit;
}

// A sessão precisa ser iniciada em cada página diferente
if (!isset($_SESSION)) session_start();

// Verifica se não há a variável da sessão que identifica o usuário
if (!isset($_SESSION['email']) && !isset($_SESSION['senha']) && !isset($_SESSION['productId'])) {
	destroiSessao();
}

if (isset($_SESSION['tempoSessao']) && (time() - $_SESSION['tempoSessao'] > 3600 )) {
	destroiSessao();
}

$_SESSION['tempoSessao'] = time();
?>